import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";
import { networkConfig } from "../config";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;

  const { deployer } = await getNamedAccounts();

  const treasury = await ethers.getContract("Treasury");

  await deploy("Issuance", {
    from: deployer,
    args: [
      treasury.address,
      networkConfig[network.name].chainlink.pairs.ETHUSD,
    ],
    log: true,
  });
};

export default func;
func.tags = ["issuance", "contract", "all"];
