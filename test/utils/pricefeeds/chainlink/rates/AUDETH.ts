import { ethers, network } from "hardhat";
import { BigNumber } from "ethers";
import { networkConfig } from "../../../../../config";
import { getAUDUSDAggregator, derivePairRate } from "../pairs";

export async function getAUDToETHRate() {
  const aggregator = await getAUDUSDAggregator();

  const decimals = await aggregator.decimals();

  return ethers.utils
    .parseEther("1")
    .mul(ethers.utils.parseUnits("1", decimals))
    .div(await getETHToAUDRate());
}

export async function getETHToAUDRate() {
  const ethToUSD = networkConfig[network.name].chainlink.pairs.ETHUSD;
  const audToUSD = networkConfig[network.name].chainlink.pairs.AUDUSD;

  return derivePairRate(ethToUSD, audToUSD);
}

export async function getAUDToETHAmount(amount: BigNumber) {
  return amount
    .mul(await getAUDToETHRate())
    .div(ethers.utils.parseUnits("1", 8));
}

export async function getETHToAUDAmount(amount: BigNumber) {
  const aggregator = await getAUDUSDAggregator();

  const decimals = await aggregator.decimals();

  return amount
    .mul(await getETHToAUDRate())
    .div(ethers.utils.parseUnits("1", decimals));
}
