import { BigNumber } from "ethers";
import { SLIPPAGE } from "./constants";

export const calculateSlippage = (amount: BigNumber) => {
  return amount.mul(SLIPPAGE).div(100);
};

export const getAmountMinusSlippage = (amount: BigNumber) => {
  return amount.sub(calculateSlippage(amount));
};
