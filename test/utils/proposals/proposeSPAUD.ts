import { ethers, network, deployments } from "hardhat";
import testProposal from "../testProposal";
import { ZERO } from "../constants";
import { BigNumber } from "ethers";
import { getTreasury, getBasket, getIssuance } from "../contracts/core";
import { networkConfig } from "../../../config";
import computeSPAUDAddress from "../contracts/computeSPAUDAddress";

export async function proposeSPAUD(initialSupply: BigNumber) {
  const basket = await getBasket();

  const targetAddresses = [basket.address, await computeSPAUDAddress()];
  const calldatas = [];
  const values = [ZERO, ZERO];

  calldatas.push(
    basket.interface.encodeFunctionData("addStableToken", [
      "SpendieAUD",
      "SPAUD",
      networkConfig[network.name].chainlink.pairs.AUDUSD,
      initialSupply,
    ])
  );

  const issuance = await getIssuance();

  const ABI = (await deployments.getArtifact("StableToken")).abi;
  const iface = new ethers.utils.Interface(ABI);

  const role = ethers.utils.solidityKeccak256(["string"], ["MINTER_ROLE"]);

  calldatas.push(
    iface.encodeFunctionData("grantRole", [role, issuance.address])
  );

  return await testProposal(
    targetAddresses,
    values,
    calldatas,
    "Create an AUD stable token"
  );
}
