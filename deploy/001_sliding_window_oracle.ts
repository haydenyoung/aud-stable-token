import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import {
  UNISWAP_V2_FACTORY,
  DEFAULT_WINDOW_SIZE,
  DEFAULT_GRANULARITY,
  networkConfig,
} from "../config";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;

  const accounts = await getNamedAccounts();

  const deployer = accounts.deployer;
  const registry = accounts.registry;

  await deploy("UniswapSlidingWindowOracle", {
    from: deployer,
    args: [
      UNISWAP_V2_FACTORY,
      DEFAULT_WINDOW_SIZE,
      DEFAULT_GRANULARITY,
      networkConfig[network.name].chainlink.upkeepRegistry || registry,
    ],
    log: true,
    waitConfirmations: networkConfig[network.name].blockConfirmations || 1,
  });
};

export default func;
func.tags = ["uniswapslidingwindoworacle", "contract", "all"];
