import { ethers } from "ethers";
import "dotenv/config";

export const WETH = "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270";

export const UNISWAP_V2_FACTORY = "0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32";
export const UNISWAP_V2_ROUTER_02 =
  "0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff";

export const DEFAULT_WINDOW_SIZE = 86400; // 24 hours
export const DEFAULT_GRANULARITY = 24; // 1 hour each

export const MIN_DELAY = 3600;

export const SPENDIEVOL_INITIAL_SUPPLY = ethers.utils.parseEther("1000000");

export const mnemonic = process.env.MNEMONIC;

export const founders = (process.env.FOUNDERS || "").split(" ");

export const developmentChains = ["hardhat", "localhost"];

export interface networkConfigItem {
  chainlink: {
    pairs: {
      ETHUSD: string;
      AUDUSD: string;
    };
    upkeepRegistry?: string;
  };
  blockConfirmations?: number;
  url: string;
}

export interface networkConfigInfo {
  [key: string]: networkConfigItem;
}

export const networkConfig: networkConfigInfo = {
  localhost: {
    chainlink: {
      pairs: {
        ETHUSD: "0xAB594600376Ec9fD91F8e885dADF0CE036862dE0",
        AUDUSD: "0x062Df9C4efd2030e243ffCc398b652e8b8F95C6f",
      },
    },
    url: process.env.POLYGON_URL || "",
  },
  hardhat: {
    chainlink: {
      pairs: {
        ETHUSD: "0xAB594600376Ec9fD91F8e885dADF0CE036862dE0",
        AUDUSD: "0x062Df9C4efd2030e243ffCc398b652e8b8F95C6f",
      },
    },
    url: process.env.POLYGON_URL || "",
  },
  mumbai: {
    chainlink: {
      pairs: {
        ETHUSD: "0xd0D5e3DB44DE05E9F294BB0a3bEEaF030DE24Ada",
        AUDUSD: "",
      },
      upkeepRegistry: "0x6179B349067af80D0c171f43E6d767E4A00775Cd",
    },
    blockConfirmations: 6,
    url: process.env.MUMBAI_URL || "",
  },
};
