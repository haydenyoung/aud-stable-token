import { expect } from "chai";
import { ethers, waffle, deployments } from "hardhat";
import { Contract } from "ethers";
import { create as createIPFS } from "ipfs-http-client";

import { getSpendieGov } from "./utils/contracts/core";

describe("SpendieGov", () => {
  let spGov: Contract;

  beforeEach(async () => {
    await deployments.fixture(["all"]);

    spGov = await getSpendieGov();
  });

  it("should have an associated metadata", async () => {
    expect(await spGov.tokenURI(0)).to.be.equal(
      "ipfs://bafybeigcvmy6fw7vg3igbxyen3uijvjwzlujmn46l3jbqy7siotj26jvty"
    );
  });

  it("should have metadata hash", async () => {
    const uri = await spGov.tokenURI(0);

    const ipfs = createIPFS();

    let metadata: any;

    for await (const file of ipfs.cat(
      uri.replace("ipfs://", "/ipfs/") + "/metadata.json"
    )) {
      const str = new TextDecoder().decode(file);

      metadata = JSON.parse(str);
    }

    const expected = {
      name: "SpendieGov NFT #1",
      description:
        "This SpendieGov token entitles the holder to a single vote in the SpendieDAO.",
      image: "bafybeifw3d234m4cttpsxehal2dlox4tl64vpi2iqqfphwwgi5odo6jwji",
    };

    expect(metadata).to.be.deep.equal(expected);
  });

  it("should have correct metadata", async () => {
    const uri = await spGov.tokenURI(0);

    const ipfs = createIPFS();

    let metadata: any;

    for await (const file of ipfs.cat(
      uri.replace("ipfs://", "/ipfs/") + "/metadata.json"
    )) {
      const str = new TextDecoder().decode(file);

      metadata = JSON.parse(str);
    }

    const expected = {
      name: "SpendieGov NFT #1",
      description:
        "This SpendieGov token entitles the holder to a single vote in the SpendieDAO.",
      image: "bafybeifw3d234m4cttpsxehal2dlox4tl64vpi2iqqfphwwgi5odo6jwji",
    };

    expect(metadata).to.be.deep.equal(expected);
  });

  it("should have image hash", async () => {
    const uri = await spGov.tokenURI(0);

    const ipfs = createIPFS();

    let metadata: any;

    for await (const file of ipfs.cat(
      uri.replace("ipfs://", "/ipfs/") + "/metadata.json"
    )) {
      const str = new TextDecoder().decode(file);

      metadata = JSON.parse(str);
    }

    expect(metadata.image).to.be.deep.equal(
      "bafybeifw3d234m4cttpsxehal2dlox4tl64vpi2iqqfphwwgi5odo6jwji"
    );
  });
});
