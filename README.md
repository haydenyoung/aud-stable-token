# Spendie.io Contracts

The core smart contracts for the Spendie.io ecosystem.

## Prerequisites

Node v16+
NPM v8+

## Installation

Clone this repository:

```
git clone https://gitlab.com/spendie/core.git
```

Install dependencies:

```
cd core
npm ci
```

## Compilation

To compile the contracts, run:

```
npm run compile
```

This will compile the contracts and build any artifacts required for contract communication. Artifacts are stored to `/artifacts`.

This is an optional step. Contracts will also be compiled when running the unit tests or deployment scripts.

## Configuration

You will need to configure an environment variables file for building, deploying and testing contracts. This file, called an .env file, will provide predefined settings to the environment during the deployment or testing process and includes contract addresses for 3rd party integration and settings for automated wallet interaction.

- Copy the provided env.example to .env,

- Open the .env file and change each setting. Use the links provided for fetching the correct contract addresses,

- Add a wallet seed phrase or mnemonic. WARNING: do not expose the seed phrase or private key to the outside world. YOUR FUNDS WILL BE STOLEN!

## Testing

A comprehensive suite of unit tests are provided to test general functionality and edge cases.

### Running Unit Tests

Unit tests are run using an integrated blockchain node. You do not need to run a local blockchain separately.

To run all unit tests, use:

```
npm run test
```

### Integrated Testing

You can run a local Hardhat node if you would like to communicate with the smart contracts from a 3rd party application (if you are building a Web3 DApp for example).

## Deploying the Contracts

### To your local dev environment

To run a Hardhat node which will work with these contracts, you need to run a fork of the Polygon mainnet blockchain:

```
npx hardhat node --fork https://url.to.mainnet/rpc/node --verbose
```

where https://url.to.mainnet/rpc/node is the URL of a valid RPC node that you can fork. You can access an RPC node using services such as Alchemy and Infura by signing up an account and setting up the required node.

When run, Hardhat Node will launch the deployment scripts (located under /deployment) to the local blockchain and the smart contracts will be available after deployment has completed. The addresses will be printed to the terminal for your convenience.

### To Testnet

TBC

### To Production

TBC

### Verifying Contracts via Etherscan

TBC
