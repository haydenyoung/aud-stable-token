// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.0 <0.9.0;

import "@openzeppelin/contracts/access/Ownable.sol";

import "./StableToken.sol";
import "./SpendieVol.sol";
import "./Treasury.sol";
import "./oracles/ISlidingWindowOracle.sol";
import "./StableTokenETHPairLibrary.sol";

/**
 * @title Redemption of stable tokens for SpendieVol.
 * @author hayden@spendie.io
 * @notice Provides a method for redeeming stable tokens for SpendieVol. Stable tokens are burned and the equivalent amount of SpendieVol is minted.
 */
contract Redemption is Ownable {
    using StableTokenETHPairLibrary for AggregatorV3Interface;

    /// The address of the treasury.
    address payable private immutable _treasury;

    /// The base USD/ETH aggregator.
    address private immutable _aggregatorUSDETH;

    /// The address of SpendieVol.
    address private immutable _spendieVol;

    /// The address of the sliding window oracle. The oracle is used for retrieving an average stable token/SpendieVol exchange rate.
    address private immutable _slidingWindowOracle;

    /// The address of the wrapped ETH contract. ETH is the generic term used to describe the native token of an EVM compatible blockchain (e.g. matic, avax, etc).
    address private immutable _WETH;

    /// The redemption fee as a fraction of 100. Default is 2%.
    uint8 public fee = 2;

    /**
     * @notice Initialize the Redemption contract.
     * @param treasury The address of the parent treasury.
     * @param slidingWindowOracle The address of the sliding window oracle.
     * @param WETH The address of the wrapped ETH contract.
     * @param spendieVol The address of the SpendieVol token.
     */
    constructor(
        address payable treasury,
        address aggregatorUSDETH,
        address slidingWindowOracle,
        address WETH,
        address spendieVol
    ) {
        _treasury = treasury;
        _aggregatorUSDETH = aggregatorUSDETH;
        _slidingWindowOracle = slidingWindowOracle;
        _WETH = WETH;
        _spendieVol = spendieVol;
    }

    /**
     * @notice Set the redemption fee to `newFee`.
     * @param newFee The new fee.
     */
    function setFee(uint8 newFee) external onlyOwner {
        require(newFee > 0 && newFee < 100, "Redemption/fee-out-of-bounds");

        uint8 oldFee = fee;
        fee = newFee;

        emit FeeSet(newFee, oldFee);
    }

    /**
     * @notice Redeem at least `minAmountOut` SpendieVol for exactly `amountIn`
     * of stable token.
     * @param tokenAddress The address of the stable token to redeem.
     * @param amountIn The exact amount of stable tokens to redeem.
     * @param minAmountOut The minimum amount of SPVOL the redeemer is willing
     * to accept.
     * @param deadline The maximum amount of time allowed for executing the
     * redemption.
     * @dev The redemption fee is applied to the amountIn before it is
     * converted to the equivalent spVol. The stable token is burned only if
     * the minAMountOut is met.
     */
    function redeemSPVOLForExactTokens(
        address tokenAddress,
        uint256 amountIn,
        uint256 minAmountOut,
        uint256 deadline
    ) external {
        require(deadline >= block.timestamp, "Redemption/deadline-expired");

        StableToken token = StableToken(tokenAddress);

        uint256 amountInMinusFee = amountIn - ((amountIn * fee) / 100);

        uint256 amountOut =
            _calculateStableTokenToSPVOL(tokenAddress, amountInMinusFee);

        require(amountOut >= minAmountOut, "Redemption/minimum-amount-not-met");

        token.burnFrom(_msgSender(), amountIn);
        SpendieVol(_spendieVol).mint(_msgSender(), amountOut);

        emit Redeemed(
            tokenAddress,
            amountIn,
            (amountIn * fee) / 100,
            amountOut
        );
    }

    /**
     * @notice Redeem exactly `exactAmountOut` SpendieVol for no more than
     * `maxAmountIn` of stable token.
     * @param tokenAddress The address of the stable token to redeem.
     * @param maxAmountIn The maximum amount of stable tokens the redeemer is
     * willing to burn.
     * @param exactAmountOut The exact amount of SPVOL the redeemer is willing
     * to accept.
     * @param deadline The maximum amount of time allowed for executing the
     * redemption.
     * @dev The redemption fee is applied to the amountIn before it is
     * converted to the equivalent spVol. The stable token is burned only if
     * the minAMountOut is met.
     */
    function redeemExactSPVOLForTokens(
        address tokenAddress,
        uint256 maxAmountIn,
        uint256 exactAmountOut,
        uint256 deadline
    ) external {
        require(deadline >= block.timestamp, "Redemption/deadline-expired");

        StableToken token = StableToken(tokenAddress);

        uint256 amountIn =
            _calculateSPVOLToStableToken(tokenAddress, exactAmountOut);

        uint256 amountInPlusFee = amountIn + ((amountIn * fee) / 100);

        require(
            amountInPlusFee <= maxAmountIn,
            "Redemption/maximum-amount-exceeded"
        );

        token.burnFrom(_msgSender(), amountInPlusFee);
        SpendieVol(_spendieVol).mint(_msgSender(), exactAmountOut);

        emit Redeemed(
            tokenAddress,
            amountInPlusFee,
            (amountIn * fee) / 100,
            exactAmountOut
        );
    }

    /**
     * @notice Calculates the amount of SpendieVol to be swapped for the
     * specified amount of stable token.
     * @param tokenAddress The address of the stable token to exchange for ETH.
     * @param amount The amount of stable tokens to exchange for SpendieVol.
     * @return The amount of SpendieVol out for stable token in.
     */
    function _calculateStableTokenToSPVOL(address tokenAddress, uint256 amount)
        internal
        view
        returns (uint256)
    {
        address priceFeed =
            Treasury(payable(_treasury)).basket().getTokenPriceFeed(
                tokenAddress
            );

        uint256 amountETH =
            uint256(
                AggregatorV3Interface(priceFeed).getTokenToETHAmount(
                    _aggregatorUSDETH,
                    int256(amount)
                )
            );

        return
            ISlidingWindowOracle(_slidingWindowOracle).consult(
                _WETH,
                amountETH,
                _spendieVol
            );
    }

    /**
     * @notice Calculates the amount of SpendieVol to be swapped for the
     * specified amount of stable token.
     * @param tokenAddress The address of the stable token to exchange for ETH.
     * @param amount The amount of SpendieVol to exchange for stable token.
     * @return The amount of stable token out for SpendieVol in.
     */
    function _calculateSPVOLToStableToken(address tokenAddress, uint256 amount)
        internal
        view
        returns (uint256)
    {
        address priceFeed =
            Treasury(payable(_treasury)).basket().getTokenPriceFeed(
                tokenAddress
            );

        uint256 amountETH =
            ISlidingWindowOracle(_slidingWindowOracle).consult(
                _spendieVol,
                amount,
                _WETH
            );

        return
            uint256(
                AggregatorV3Interface(priceFeed).getETHToTokenAmount(
                    _aggregatorUSDETH,
                    int256(amountETH)
                )
            );
    }

    /**
     * @dev Fired when SPVOL is successfully redeemed.
     * @param tokenAddress The address of the token to redeem/burn.
     * @param amountIn The amount of stable tokens being redeemed/burned.
     * @param fee The fee incurred to the redemption.
     * @param amountOut The amount of SPVOL issued to the redeemer.
     */
    event Redeemed(
        address indexed tokenAddress,
        uint256 amountIn,
        uint256 fee,
        uint256 amountOut
    );

    /**
     * @dev Sets the redemption fee. Only the owner of the Redemption contract
     * can change the redemption fee.
     * @param newFee The new fee.
     * @param oldFee The previous fee.
     */
    event FeeSet(uint8 newFee, uint8 oldFee);
}
