import { ethers } from "hardhat";
import { getAUDUSDAggregator } from "../pairs";

export const getAUDToUSDRate = async () => {
  const aggregator = await getAUDUSDAggregator();
  const roundData = await aggregator.latestRoundData();

  return roundData.answer;
};

export const getUSDToAUDRate = async () => {
  const aggregator = await getAUDUSDAggregator();

  const decimals = await aggregator.decimals();

  return ethers.utils
    .parseUnits("1", decimals)
    .mul(ethers.utils.parseUnits("1", decimals))
    .div(await getAUDToUSDRate());
};
