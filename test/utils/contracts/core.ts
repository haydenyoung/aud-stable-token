import { ethers, deployments, waffle } from "hardhat";
import { Contract } from "ethers";

export async function getGovernor(): Promise<Contract> {
  return await ethers.getContract(
    "SpendieGovernor",
    waffle.provider.getSigner()
  );
}

export async function getTreasury(): Promise<Contract> {
  return await ethers.getContract("Treasury", waffle.provider.getSigner());
}

export async function getBasket(): Promise<Contract> {
  const basketAddress = await (await getTreasury()).basket();

  const artifact = await deployments.getArtifact("Basket");

  const basket = new ethers.Contract(
    basketAddress,
    artifact.abi,
    waffle.provider.getSigner()
  );

  return basket;
}

export async function getSpendieGov(): Promise<Contract> {
  return await ethers.getContract("SpendieGov", waffle.provider.getSigner());
}

export async function getSpendieVol(): Promise<Contract> {
  return await ethers.getContract("SpendieVol", waffle.provider.getSigner());
}

export async function getIssuance(): Promise<Contract> {
  return await ethers.getContract("Issuance", waffle.provider.getSigner());
}

export async function getRedemption(): Promise<Contract> {
  return await ethers.getContract("Redemption", waffle.provider.getSigner());
}
