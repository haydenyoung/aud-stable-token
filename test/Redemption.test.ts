import { expect } from "chai";
import { ethers, waffle, deployments } from "hardhat";
import { BigNumber, Contract } from "ethers";

import {
  getTreasury,
  getBasket,
  getIssuance,
  getSpendieVol,
  getRedemption,
} from "./utils/contracts/core";
import { getUniswapV2Router02 } from "./utils/contracts/periphery";
import { getTwentyMinuteDeadline } from "./utils/deadline";
import { proposeSPAUD } from "./utils/proposals/proposeSPAUD";
import { proposeAddSPVolETHLiquidity } from "./utils/proposals/proposeAddSPVolETHLiquidity";

import * as Issuance from "./utils/issuance";
import * as Redemption from "./utils/redemption";
import * as AUDETHPair from "./utils/pricefeeds/chainlink/rates/AUDETH";

import { hour } from "./utils/constants";
import { advanceBlockTimestamp } from "./utils/mining";
import testProposal from "./utils/testProposal";
import updateSlidingWindow from "./utils/pricefeeds/updateSlidingWindow";

describe("Redemption", () => {
  const initialSupply = BigNumber.from(1).mul(BigNumber.from(10).pow(18));

  let treasury: any;
  let basket: any;
  let oracle: any;
  let issuance: any;
  let redemption: any;
  let uniswapV2Router02: any;
  let spVol: any;

  let provider: any;

  before(async () => {
    provider = waffle.provider;
  });

  beforeEach(async () => {
    await deployments.fixture(["all"]);

    oracle = await ethers.getContract("UniswapSlidingWindowOracle");

    treasury = await getTreasury();
    basket = await getBasket();
    issuance = await getIssuance();
    redemption = await getRedemption();
    spVol = await getSpendieVol();

    const tx = {
      to: treasury.address,
      value: ethers.utils.parseEther("100"),
    };

    await provider.getSigner().sendTransaction(tx);

    const amountETHIn = ethers.utils.parseEther("1");
    const amountAUDOut = await AUDETHPair.getETHToAUDAmount(amountETHIn);
    const minAmountAUDOut = Issuance.getAmountMinusIssuanceFee(amountAUDOut);

    const options = {
      value: amountETHIn,
    };

    uniswapV2Router02 = await getUniswapV2Router02();

    await proposeAddSPVolETHLiquidity(uniswapV2Router02);

    await proposeSPAUD(initialSupply);

    await issuance.issueTokensForExactETH(
      basket.getStableToken(0),
      minAmountAUDOut,
      getTwentyMinuteDeadline(),
      options
    );

    await updateSlidingWindow();

    await advanceBlockTimestamp(provider, 23 * hour);
  });

  describe("Redeem SPVOL for exact stable tokens", () => {
    let spAUD: Contract;
    let amountAUDIn: BigNumber;
    let minAmountSPVOLOut: BigNumber;

    beforeEach(async () => {
      const spAUDAddress = await basket.getStableToken(0);
      spAUD = new Contract(
        spAUDAddress,
        (await deployments.getArtifact("StableToken")).abi,
        provider.getSigner()
      );

      amountAUDIn = ethers.utils.parseUnits("1", "8");

      // Give the redemption contract approval to burn redeemer's tokens.
      await spAUD.approve(redemption.address, amountAUDIn);

      const spVol = await getSpendieVol();

      minAmountSPVOLOut = await oracle.consult(
        uniswapV2Router02.WETH(),
        await AUDETHPair.getAUDToETHAmount(
          Redemption.getAmountMinusRedemptionFee(amountAUDIn)
        ),
        spVol.address
      );
    });

    it("should burn 1 SpendieAUD and send SpendieVol to user", async () => {
      const spVolExpected = minAmountSPVOLOut;

      await redemption.redeemSPVOLForExactTokens(
        spAUD.address,
        amountAUDIn,
        minAmountSPVOLOut,
        getTwentyMinuteDeadline()
      );

      expect(
        await spVol.balanceOf(await provider.getSigner().getAddress())
      ).to.be.equal(spVolExpected);
    });

    it("should emit the event Redeemed when redeeming min amount out", async () => {
      const expectedFees = Redemption.calculateRedemptionFee(amountAUDIn);

      await expect(
        redemption.redeemSPVOLForExactTokens(
          spAUD.address,
          amountAUDIn,
          minAmountSPVOLOut,
          getTwentyMinuteDeadline()
        )
      )
        .to.emit(redemption, "Redeemed")
        .withArgs(
          await basket.getStableToken(0),
          amountAUDIn,
          expectedFees,
          minAmountSPVOLOut
        );
    });

    it("should revert when min amount out not met", async () => {
      const uniswapV2Router02 = await getUniswapV2Router02();

      const amountIn = ethers.utils.parseEther("1");

      const amountsOut = await uniswapV2Router02.getAmountsOut(amountIn, [
        uniswapV2Router02.WETH(),
        spVol.address,
      ]);

      const minAmountOut = amountsOut[1];

      await oracle.consult(
        uniswapV2Router02.WETH(),
        await AUDETHPair.getAUDToETHAmount(
          Redemption.getAmountMinusRedemptionFee(amountAUDIn)
        ),
        spVol.address
      );

      await uniswapV2Router02.swapExactETHForTokens(
        minAmountOut,
        [uniswapV2Router02.WETH(), spVol.address],
        provider.getSigner().getAddress(),
        getTwentyMinuteDeadline(),
        { value: amountIn }
      );

      await updateSlidingWindow();

      await oracle.consult(
        uniswapV2Router02.WETH(),
        await AUDETHPair.getAUDToETHAmount(
          Redemption.getAmountMinusRedemptionFee(amountAUDIn)
        ),
        spVol.address
      );

      await expect(
        redemption.redeemSPVOLForExactTokens(
          spAUD.address,
          amountAUDIn,
          minAmountSPVOLOut,
          getTwentyMinuteDeadline()
        )
      ).to.be.revertedWith("Redemption/minimum-amount-not-met");
    });

    it("should revert when deadline expires", async () => {
      const deadline = await getTwentyMinuteDeadline();
      await advanceBlockTimestamp(provider, hour); // advance 1 hour to expire.

      await expect(
        redemption.redeemSPVOLForExactTokens(
          spAUD.address,
          amountAUDIn,
          minAmountSPVOLOut,
          deadline
        )
      ).to.be.revertedWith("Redemption/deadline-expired");
    });
  });

  describe("Redeem exact SPVOL for stable tokens", () => {
    let spAUD: Contract;
    let amountAUDIn: BigNumber;
    let maxAmountAUDIn: BigNumber;
    let exactAmountSPVOLOut: BigNumber;

    beforeEach(async () => {
      const spAUDAddress = await basket.getStableToken(0);
      spAUD = new Contract(
        spAUDAddress,
        (await deployments.getArtifact("StableToken")).abi,
        provider.getSigner()
      );

      exactAmountSPVOLOut = ethers.utils.parseUnits("1", "8");

      const spVol = await getSpendieVol();

      amountAUDIn = await AUDETHPair.getETHToAUDAmount(
        await oracle.consult(
          spVol.address,
          exactAmountSPVOLOut,
          uniswapV2Router02.WETH()
        )
      );

      maxAmountAUDIn = Redemption.getAmountPlusRedemptionFee(amountAUDIn);

      // Give the redemption contract approval to burn redeemer's tokens.
      await spAUD.approve(redemption.address, maxAmountAUDIn);
    });

    it("should burn SpendieAUD and send exactly 1 SpendieVol to user", async () => {
      const spVolExpected = exactAmountSPVOLOut;

      await redemption.redeemExactSPVOLForTokens(
        spAUD.address,
        maxAmountAUDIn,
        exactAmountSPVOLOut,
        getTwentyMinuteDeadline()
      );

      expect(
        await spVol.balanceOf(await provider.getSigner().getAddress())
      ).to.be.equal(spVolExpected);
    });

    it("should emit the event Redeemed when redeeming exact amount out", async () => {
      const expectedFees = Redemption.calculateRedemptionFee(amountAUDIn);

      await expect(
        redemption.redeemExactSPVOLForTokens(
          spAUD.address,
          maxAmountAUDIn,
          exactAmountSPVOLOut,
          getTwentyMinuteDeadline()
        )
      )
        .to.emit(redemption, "Redeemed")
        .withArgs(
          await basket.getStableToken(0),
          maxAmountAUDIn,
          expectedFees,
          exactAmountSPVOLOut
        );
    });

    it("should revert when max amount in exceeded", async () => {
      const uniswapV2Router02 = await getUniswapV2Router02();

      const amountIn = ethers.utils.parseEther("1");

      const amountsOut = await uniswapV2Router02.getAmountsOut(amountIn, [
        uniswapV2Router02.WETH(),
        spVol.address,
      ]);

      const minAmountOut = amountsOut[1];

      await oracle.consult(
        uniswapV2Router02.WETH(),
        await AUDETHPair.getAUDToETHAmount(
          Redemption.getAmountMinusRedemptionFee(maxAmountAUDIn)
        ),
        spVol.address
      );

      await uniswapV2Router02.swapExactETHForTokens(
        minAmountOut,
        [uniswapV2Router02.WETH(), spVol.address],
        provider.getSigner().getAddress(),
        getTwentyMinuteDeadline(),
        { value: amountIn }
      );

      await updateSlidingWindow();

      await oracle.consult(
        uniswapV2Router02.WETH(),
        await AUDETHPair.getAUDToETHAmount(
          Redemption.getAmountMinusRedemptionFee(maxAmountAUDIn)
        ),
        spVol.address
      );

      await expect(
        redemption.redeemExactSPVOLForTokens(
          spAUD.address,
          maxAmountAUDIn,
          exactAmountSPVOLOut,
          getTwentyMinuteDeadline()
        )
      ).to.be.revertedWith("Redemption/maximum-amount-exceeded");
    });

    it("should revert when deadline expires", async () => {
      const deadline = await getTwentyMinuteDeadline();
      await advanceBlockTimestamp(provider, hour); // advance 1 hour to expire.

      await expect(
        redemption.redeemExactSPVOLForTokens(
          spAUD.address,
          maxAmountAUDIn,
          exactAmountSPVOLOut,
          deadline
        )
      ).to.be.revertedWith("Redemption/deadline-expired");
    });
  });

  describe("fees", () => {
    it("should set a new fee", async () => {
      const targetAddresses = [redemption.address];
      const values = [BigNumber.from(0)];
      const callDatas = [];
      callDatas.push(redemption.interface.encodeFunctionData("setFee", ["3"]));
      const description = "Change redemption fee to 3%";

      await testProposal(targetAddresses, values, callDatas, description);

      expect(await redemption.fee()).to.be.equal(3);
    });

    it("should emit the event FeeSet when changing the fee", async () => {
      const expectedNewFee = 3;
      const expectedOldFee = 2;

      const targetAddresses = [redemption.address];
      const values = [BigNumber.from(0)];
      const callDatas = [];
      callDatas.push(
        redemption.interface.encodeFunctionData("setFee", [
          expectedNewFee.toString(),
        ])
      );
      const description = "Change redemption fee to 3%";

      expect(
        await testProposal(targetAddresses, values, callDatas, description)
      )
        .to.emit(redemption, "FeeSet")
        .withArgs(expectedNewFee, expectedOldFee);
    });

    it("should not be able to set a fee equal to or below 0", async () => {
      const targetAddresses = [redemption.address];
      const values = [BigNumber.from(0)];
      const callDatas = [];
      callDatas.push(redemption.interface.encodeFunctionData("setFee", ["0"]));
      const description = "Change redemption fee to 0%";

      await expect(
        testProposal(targetAddresses, values, callDatas, description)
      ).to.be.revertedWith(
        "TimelockController: underlying transaction reverted"
      );
    });

    it("should not be able to set a fee equal to or above 100", async () => {
      const targetAddresses = [redemption.address];
      const values = [BigNumber.from(0)];
      const callDatas = [];
      callDatas.push(
        redemption.interface.encodeFunctionData("setFee", ["100"])
      );
      const description = "Change redemption fee to 100%";

      await expect(
        testProposal(targetAddresses, values, callDatas, description)
      ).to.be.revertedWith(
        "TimelockController: underlying transaction reverted"
      );
    });
  });
});
