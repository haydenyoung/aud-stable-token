import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";
import { networkConfig, WETH } from "../config";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts, network } = hre;
  const { deploy } = deployments;

  const { deployer } = await getNamedAccounts();

  const treasury = await ethers.getContract("Treasury");
  const oracle = await ethers.getContract("UniswapSlidingWindowOracle");
  const spendieVol = await ethers.getContract("SpendieVol");

  await deploy("Redemption", {
    from: deployer,
    args: [
      treasury.address,
      networkConfig[network.name].chainlink.pairs.ETHUSD,
      oracle.address,
      WETH,
      spendieVol.address,
    ],
  });
};

export default func;
func.tags = ["redemption", "contract", "all"];
