// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.8.0 <0.9.0;

import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

/**
 * @title A base price feed for price pairs.
 * @author hayden@spendie.io
 * @notice Provides a common set of methods for obtaining token to ether exchange rates. ETH is the generic term used to describe the native token of an EVM compatible blockchain (e.g. matic, avax, etc).
 */
library StableTokenETHPairLibrary {
    function getTokenToETHRate(AggregatorV3Interface self, address base)
        internal
        view
        returns (int256)
    {
        return
            (1e18 * int256(10**AggregatorV3Interface(base).decimals())) /
            getETHToTokenRate(self, base);
    }

    function getTokenToETHAmount(
        AggregatorV3Interface self,
        address base,
        int256 amount
    ) internal view returns (int256) {
        return
            (amount * getTokenToETHRate(self, base)) /
            int256(10**self.decimals());
    }

    function getETHToTokenRate(AggregatorV3Interface self, address base)
        internal
        view
        returns (int256)
    {
        (, int256 ethToUSD, , , ) =
            AggregatorV3Interface(base).latestRoundData();
        (, int256 tokenToUSD, , , ) = self.latestRoundData();

        return (ethToUSD * int256(10**self.decimals())) / tokenToUSD;
    }

    function getETHToTokenAmount(
        AggregatorV3Interface self,
        address base,
        int256 amount
    ) internal view returns (int256) {
        return
            (amount * getETHToTokenRate(self, base)) /
            int256(10**AggregatorV3Interface(base).decimals());
    }
}
