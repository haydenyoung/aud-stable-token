import { HardhatRuntimeEnvironment } from "hardhat/types";
import { DeployFunction } from "hardhat-deploy/types";
import { ethers } from "hardhat";
import { MIN_DELAY } from "../config";

const func: DeployFunction = async function (hre: HardhatRuntimeEnvironment) {
  const { deployments, getNamedAccounts } = hre;
  const { deploy } = deployments;

  const { deployer } = await getNamedAccounts();

  const spendieGov = await ethers.getContract("SpendieGov");

  const treasury = await deploy("Treasury", {
    from: deployer,
    args: [MIN_DELAY, [], []],
    log: true,
  });

  await deploy("SpendieGovernor", {
    from: deployer,
    args: [spendieGov.address, treasury.address],
  });
};

export default func;
func.tags = ["treasury", "governor", "contract", "all"];
