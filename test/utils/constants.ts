import { ethers } from "hardhat";
import { BigNumber } from "ethers";

export enum VoteType {
  Against,
  For,
  Abstain,
}

export enum ProposalState {
  Pending,
  Active,
  Canceled,
  Defeated,
  Succeeded,
  Queued,
  Expired,
  Executed,
}

export const ZERO = ethers.BigNumber.from(0);

export const SALT = Number(1).toString(16);

export const hour = 3600;

export const ISSUANCE_FEE = BigNumber.from("2"); // as a percentage, I.e. 2%.

export const REDEMPTION_FEE = BigNumber.from("2"); // as a percentage, I.e. 2%.

export const SLIPPAGE = BigNumber.from("1"); // as a percentage, I.e. 1%;

export const SpendieAUD = { name: "SpendieAUD", symbol: "SPAUD" };
