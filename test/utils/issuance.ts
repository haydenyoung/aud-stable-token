import { BigNumber } from "ethers";
import { ISSUANCE_FEE } from "./constants";

export function calculateIssuanceFee(amount: BigNumber): BigNumber {
  return amount.mul(ISSUANCE_FEE).div(100);
}

export const getAmountPlusIssuanceFee = (amount: BigNumber) => {
  return amount.add(calculateIssuanceFee(amount));
};

export const getAmountMinusIssuanceFee = (amount: BigNumber) => {
  return amount.sub(calculateIssuanceFee(amount));
};
