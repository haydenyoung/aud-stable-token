import { ethers, waffle, deployments } from "hardhat";
import { getBasket } from "./core";
import { SpendieAUD } from "../constants";

export default async () => {
  const basket = await getBasket();
  const abiCoder = ethers.utils.defaultAbiCoder;

  const from = basket.address;
  const salt = ethers.utils.solidityKeccak256(
    ["string", "string"],
    [SpendieAUD.name, SpendieAUD.symbol]
  );

  const initCode = (await deployments.getArtifact("StableToken")).bytecode;

  const encodedParams = abiCoder.encode(
    ["string", "string"],
    [SpendieAUD.name, SpendieAUD.symbol]
  );

  const initCodeHash = ethers.utils.solidityKeccak256(
    ["bytes", "bytes"],
    [initCode, encodedParams]
  );

  return ethers.utils.getCreate2Address(from, salt, initCodeHash);
};
