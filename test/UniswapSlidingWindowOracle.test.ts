import "dotenv/config";
import { expect } from "chai";
import { ethers, waffle, deployments } from "hardhat";

import { getTreasury, getSpendieVol } from "./utils/contracts/core";
import { getUniswapV2Router02 } from "./utils/contracts/periphery";
import { advanceBlockTimestamp } from "./utils/mining";
import { proposeAddSPVolETHLiquidity } from "./utils/proposals/proposeAddSPVolETHLiquidity";
import { hour } from "./utils/constants";
import updateSlidingWindow from "./utils/pricefeeds/updateSlidingWindow";

describe("UniswapSlidingWindowOracle", () => {
  let oracle: any;
  let treasury: any;
  let spVol: any;
  let uniswapV2Router02: any;

  let provider: any;

  before(async () => {
    provider = waffle.provider;
  });

  beforeEach(async () => {
    await deployments.fixture(["all"]);

    oracle = await ethers.getContract("UniswapSlidingWindowOracle");

    treasury = await getTreasury();

    const tx = {
      to: treasury.address,
      value: ethers.utils.parseEther("100"),
    };

    await provider.getSigner().sendTransaction(tx);

    spVol = await getSpendieVol();

    uniswapV2Router02 = await getUniswapV2Router02();

    // we need a uniswap LP pair to be able to query so let's deploy one.
    await proposeAddSPVolETHLiquidity(uniswapV2Router02);

    await updateSlidingWindow();

    // move to hour 23:00 so we can check prices
    await advanceBlockTimestamp(provider, 23 * hour);
  });

  it("should query oracle for price", async () => {
    expect(
      await oracle.consult(spVol.address, 1, uniswapV2Router02.WETH())
    ).to.be.equal(1);
  });

  it("should not allow a non-registry account to perform upkeep", async () => {
    const abiCoder = new ethers.utils.AbiCoder();

    await expect(
      oracle
        .connect(provider.getSigner())
        .performUpkeep(
          abiCoder.encode(
            ["address", "address"],
            [spVol.address, await uniswapV2Router02.WETH()]
          )
        )
    ).to.be.revertedWith("UniswapSlidingWindowOracle/only-registry");
  });

  it("should be paused", async () => {
    oracle.pause();

    expect(await oracle.paused()).to.be.true;
  });

  it("should be unpaused", async () => {
    oracle.pause();
    oracle.unpause();

    expect(await oracle.paused()).to.be.false;
  });

  it("should emit Paused event", async () => {
    await expect(oracle.pause()).to.emit(oracle, "Paused");
  });

  it("should emit Unpaused event", async () => {
    await oracle.pause();
    await expect(oracle.unpause()).to.emit(oracle, "Unpaused");
  });

  it("should not perform upkeep if paused", async () => {
    const abiCoder = new ethers.utils.AbiCoder();

    oracle.pause();

    const [, , registry] = await ethers.getSigners();

    await expect(
      oracle
        .connect(registry)
        .performUpkeep(
          abiCoder.encode(
            ["address", "address"],
            [spVol.address, await uniswapV2Router02.WETH()]
          )
        )
    ).to.be.revertedWith("Pausable: paused");
  });
});
