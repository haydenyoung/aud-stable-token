export async function mineNBlocks(provider: any, blocks: Number) {
  for (let index = 0; index < blocks; index++) {
    await provider.send("evm_mine");
  }
}

export async function advanceBlockTimestamp(
  provider: any,
  newTimestamp: Number
) {
  await provider.send("evm_increaseTime", [newTimestamp]);
  await provider.send("evm_mine");
}
